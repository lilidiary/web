(function ($) {

  var nav_offset_top = $("header").height() + 50;

  function navbarFixed() {
    if ($(".header_area").length) {
      $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= nav_offset_top) {
          $(".header_area").addClass("navbar_fixed");
        } else {
          $(".header_area").removeClass("navbar_fixed");
        }
      });
    }
  }
  navbarFixed();
})(jQuery);

ymaps.ready(init);

function init() {
  const map = new ymaps.Map("map", {
    center: [60, 75],
    zoom: 4,
    type: "yandex#map",
    controls: []
  });
  map.behaviors.disable("scrollZoom");
  // map.behaviors.disable("drag");

  ymaps.borders
    .load("RU", {
      lang: "ru",
      quality: 3
    })
    .then(function (geojson) {
      for (const reg of geojson.features) {
        let add = false;
        for (const obl of regs) {
          if (
            obl.name.toLocaleLowerCase() === reg.properties.name.toLowerCase()
          ) {
            add = true;
          }
        }
        if (add) {
          var geoObject = new ymaps.GeoObject(reg);
          map.geoObjects.add(geoObject);
          geoObject.events.add("click", () => {
            document.location = "https://vk.me/lildiarybot";
          });
        }
      }
    });

  for (const obl of regs) {
    ymaps
      .geocode(obl.name, {
        results: 1
      })
      .then(function (res) {
        const firstGeoObject = res.geoObjects.get(0);

        firstGeoObject.options.set("preset", "islands#circleIcon");
        firstGeoObject.properties.set(
          "iconCaption",
          obl.name
        );

        map.geoObjects.add(firstGeoObject);
      });
  }
}
